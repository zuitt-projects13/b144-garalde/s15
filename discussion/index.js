// operators
/*
	Add + sum
	Subtract - difference
	Multiply * product
	Divide / quotient
	Modulus % remainder

*/

function mod() {
	return 9 % 2;
}

console.log(mod());


// assignment operator


/*
	+= addition
	-= subtraction
	*= multiplication
	/= division
	%= modulo
*/

let sum	= 1;
// sum = sum + 1;
sum += 1;

console.log(sum)


// increment and decrement
// ++ add 1
// -- minus 1

let z=1;

let increment = ++z;


console.log("result of pre-increment: " + increment)
console.log("result of pre-increment: " + z)



// post-increment

increment = z++;
console.log("result of post-increment: " + increment)
console.log("result of post-increment: " + z)

console.log("=================")
// pre-decrement
let	decrement = --z;
console.log("Result of pre-decrement:" + decrement)
console.log("Result of pre-decrement:" +z)


decrement = z--;
console.log("Result of post-decrement:" + decrement)
console.log("Result of post-decrement:" +z)



// comparison operators

// equality operand

let	juan = 'juan'

console.log(1 == 1)
console.log(0 == false)
console.log('juan' == juan);
// strict equality
console.log(1 === true)

// inequality operator
console.log("inequality operator")
console.log(1 != 1)
console.log('Juan' != juan);

// strict inequality (!==)
console.log (0 !== false)


// logical operators
console.log("logical operator")

let isLegalAge = true;
let isRegistered = false;




/*
	&& And symbo.l both shoulg be TRUE
	|| OR operator. at least 1 should be true
*/


// and opetator
let allRequirementsMet = isLegalAge && isRegistered;

console.log("Result of logical AND operator:" + allRequirementsMet)

// or operator
let someRequirementsMet = isLegalAge || isRegistered
console.log("Result of logical OR operator:" + someRequirementsMet)



// selection control structure

// IF statement - executes a statement if a specified condition  is true

let num = -1;

if (num<0) {
	console.log('Hello')
}

// mini activity

let val=15
if (val>=10) {

	console.log("Welcome to Zuit")

}

// IF ELSE Statement - executes a statement if the previous condition returns false

/*
if else syntax
if (condition) {
	statement
}

else {
	statement
}*/

num=5;
if (num>=10) {
	console.log("Number is greater than or equal to 10")

}
else {
	console.log("number is not greater or equal to 10")
}


// mini activity
/*let age = parseInt(prompt("Please provde age: "));
if (age>59) {
	alert("Senior Age")

}
else {
	alert("Invalid Age")
}
*/

// IF ELSEIF ELSE statement

/*let city = parseInt(prompt("Enter a number: "))
if (city===1) {
	alert("Welcome to Quezon City")
}
else if (city===2) {
	alert("Welcome to Valenzuela City")
}
else if (city===3) {
	alert("Welcome to Pasig City")
}
else if (city===4) {
	alert("Welcome to Taguig")
}

else {
	 alert("Invalid Number")
}*/	


// -------------------------
/*let message = ''

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed<30){
		return 'Not a Typhoon yet'
	}

	else if (windSpeed <=61) {
		return 'Tropical depression detected'
	}

	else if (windSpeed >=61 && windSpeed <= 88) {
		return 'Tropical storm detected'
	}

	else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected'
	}
 	
 	else {
 		return 'Typhoon detected'
 	}

}

message = determineTyphoonIntensity(70);

console.log(message)*/



// Ternary operator
/*
	syntax
	(condition)? ifTrue : ifFalse

*/

let ternaryResult = (1 < 18) ? 'valid' : 'invalid';
// if condition is true 'valid will be returned. else invalid'
console.log('Result of ternary operator: ' + ternaryResult)


function isOfLegalAge() {
	name = 'John';
	return 'You are of legal age limit'
}
function isUnderAge() {
	name = 'Jane';
	return 'You are unde the age limit'
}


let age = parseInt(prompt("What is your age?"))

let legalage = (age >= 18) ? isOfLegalAge() : isUnderAge();

alert('Result of ternary operator in functions: ' + legalage + ',' + name)


// SWITCH STATEMENT
/*
		switch (expression) {
			case value1:
				statement;
				break
			default:
			statement;	
		}

*/

let day = prompt("What day of the week is it today?").toLowerCase();

switch(day) {
		case 'sunday' :
				alert('The color of the day is red')
				break;
		case 'monday' :
				alert('The color of the day is orange')
				break;
		case 'tuesday' :
				alert('The color of the day is yellow')
				break;					
		case 'wednesday' :
				alert('The color of the day is green')
				break;
		case 'thursday' :
				alert('The color of the day is blue')
				break;
		case 'friday' :
				alert('The color of the day is indigo')
				break;							
		case 'saturday' :
				alert('The color of the day is violet')
				break;
		default:	
				alert('Please input valid day')	
}						



// try-catch-finally statement
// commonly used for error handling
// try - if gagana yung code
// catch - if may error may ilalabas na message
// finally - whether may error or not gagawin nya yung statement



function showIntensityAlert(windSpeed)
{
	try {
		alertat(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error)
		console.warn(error.message)
	}

	finally {
		alert('Intensity updates  will show you alert')
	}
}

showIntensityAlert(56);













